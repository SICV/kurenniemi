#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os, sys, Image
from time import sleep


try:
    inpath = sys.argv[1]
except IndexError:
    inpath = "."
inpath = os.path.abspath(inpath)

try:
    os.mkdir("out")
except OSError:
    pass

def isImage (p):
    try:
        i = Image.open(p)
        return True
    except IOError:
        return False

# Take a walk!
counter = 0
for root, dirs, files in os.walk(inpath):
    # skip any dot directories
    for d in dirs:
        if d.startswith("."):
            dirs.remove(d)
    print "# Procesing:" + root
    imgs = []
    for f in files:
        # skip any dot files
        if f.startswith("."): continue
        apath = os.path.abspath(os.path.join(root, f))
        if isImage(apath):
            imgs.append(apath)
    # print imgs
    if len(imgs):
        counter += 1
        opath = "out/montage%04d.png" % counter
        cmd = """
montage %(FILES)s %(OPATH)s
mogrify -resize 480x\> %(OPATH)s
""".strip() % {
    'FILES' : " ".join(['"%s"' % x for x in imgs]),
    'OPATH' : opath
}
        print cmd
        print
        os.system(cmd)
        print '# display "%s"' % opath
        print
        sleep(1)

