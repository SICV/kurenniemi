#include <iostream>
#include"stdio.h"
#include <QDir>
#include <cv.h>
#include <highgui.h>
#include <QDateTime>

using namespace std;

void RecurseDirectory(const QString& sDir)
{
    QDir dir(sDir);
    QFileInfoList list = dir.entryInfoList();
    for (int iList=0;iList<list.count();iList++)
    {
        QFileInfo info = list[iList];

        QString sFilePath = info.filePath();
        if (info.isDir())
        {
            // recursive
            if (info.fileName()!=".." && info.fileName()!=".")
            {
                RecurseDirectory(sFilePath);
            }
        }
        else
        {
            // Do something with the file here
            std::cout << qPrintable(QString("%1 %2").arg(info.size(), 10)
                                    .arg(info.absoluteFilePath()));
            std::cout << std::endl;
        }
    }
}



int main(int argc, char *argv[]){

    int cond=1;

    if(cond>0){
        int dirimages=0;

        //    QDir dir=QDir::currentPath();
        QDir dir=QDir("./img");
        dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
        dir.setSorting(QDir::Name);

        QFileInfoList list = dir.entryInfoList();
        int dp=list.size()+1;
        IplImage* imgRed[dp];
        IplImage* imgGreen[dp];
        IplImage* imgBlue[dp];
        int xmax=0;
        int ymax=0;
        int curx, cury;

        for (int i = 0; i < list.size(); ++i)
        {
            IplImage* img;
            QFileInfo fileInfo = list.at(i);
            std::cout << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10)
                                    .arg(fileInfo.absoluteFilePath()));
            std::cout << std::endl;
            img = cvLoadImage(qPrintable(fileInfo.absoluteFilePath()));
            if(img){
                dirimages++;
                curx=cvGetSize(img).width;
                cury=cvGetSize(img).height;
                if(curx>xmax){
                    xmax=curx;
                }
                if(cury>ymax){
                    ymax=cury;
                }
                imgRed[i] = cvCreateImage(cvGetSize(img), 8, 1);
                imgGreen[i] = cvCreateImage(cvGetSize(img), 8, 1);
                imgBlue[i] = cvCreateImage(cvGetSize(img), 8, 1);
                cvSplit(img, imgRed[i], imgGreen[i], imgBlue[i], NULL);
                std::cout<<"passed"<<"\n";

                cvReleaseImage(&img);
            }
        }

        if(dirimages>0){
            // CvSize imgSize = cvGetSize(imgRed[0]);
            CvSize imgSize = cvSize(xmax,ymax);
            IplImage* imgZero = cvCreateImage(imgSize, 8, 1);
            IplImage* imgResultRed = cvCreateImage(imgSize, 8, 1);
            IplImage* imgResultGreen = cvCreateImage(imgSize, 8, 1);
            IplImage* imgResultBlue = cvCreateImage(imgSize, 8, 1);
            IplImage* imgDisplayResultRed = cvCreateImage(imgSize, 8, 3);
            IplImage* imgDisplayResultGreen = cvCreateImage(imgSize, 8, 3);
            IplImage* imgDisplayResultBlue = cvCreateImage(imgSize, 8, 3);

            IplImage* imgResult = cvCreateImage(imgSize, 8, 3);

            for(int y=0;y<imgSize.height;y++)
            {
                for(int x=0;x<imgSize.width;x++)
                {
                    int theSumRed=0;
                    int theSumGreen=0;
                    int theSumBlue=0;
                    int theZeroSum=0;
                    for(int i=0;i<dirimages;i++)
                    {
                        int coefy=(imgSize.height-imgRed[i]->height)/2;
                        int coefx=(imgSize.width-imgRed[i]->width)/2;
                        //                if(imgRed[i]->height>y && imgRed[i]->width>x){
                        if((y<coefy) || (x<coefx)||(y>=coefy+imgRed[i]->height)||(x>=coefx+imgRed[i]->width)){
                            theSumRed+=255.0;
                            theSumGreen+=255.0;
                            theSumBlue+=255.0;
                        }else{
//                            if(cvGetReal2D(imgRed[i], (y-coefy), (x-coefx))<60.0){
                            theSumRed+=cvGetReal2D(imgRed[i], (y-coefy), (x-coefx));
//                            }else{
//                            theSumRed+=60.0;

//                            }
                            theSumGreen+=cvGetReal2D(imgGreen[i],(y-coefy), (x-coefx));
                            theSumBlue+=cvGetReal2D(imgBlue[i],(y-coefy), (x-coefx));
                        }
                    }
                    theSumRed = (float)theSumRed/dirimages;
                    theSumGreen = (float)theSumGreen/dirimages;
                    theSumBlue = (float)theSumBlue/dirimages;
                    cvSetReal2D(imgResultRed, y, x, theSumRed);
                    cvSetReal2D(imgResultGreen, y, x, theSumGreen);
                    cvSetReal2D(imgResultBlue, y, x, theSumBlue);
                    cvSetReal2D(imgZero, y, x, theZeroSum);

                }
            }
            cvMerge(imgResultRed, imgResultGreen, imgResultBlue, NULL, imgResult);
            cvMerge(imgResultRed, imgZero, imgZero, NULL, imgDisplayResultRed);
            cvMerge(imgZero,imgResultGreen, imgZero, NULL, imgDisplayResultGreen);
            cvMerge(imgZero,  imgZero, imgResultBlue, NULL, imgDisplayResultBlue);

            QDateTime date = QDateTime::currentDateTime();
            QString imageDateString = date.toString("yyyy-MM-dd_hh-mm-ss");
            QString imageDateStringRed=imageDateString.append("-red-average.jpg");
            QString imageDateStringGreen=imageDateString.append("-green-average.jpg");
            QString imageDateStringBlue=imageDateString.append("-blue-average.jpg");

            imageDateString.append("-average.jpg");
            //    string ids=qPrintable(imageDateString);
            cvSaveImage(qPrintable(imageDateString),imgResult);
            cvSaveImage(qPrintable(imageDateStringRed),imgDisplayResultRed);
            cvSaveImage(qPrintable(imageDateStringGreen),imgDisplayResultGreen);
            cvSaveImage(qPrintable(imageDateStringBlue),imgDisplayResultBlue);
            cvNamedWindow("averaged");
            cvShowImage("averaged", imgResult);
            cvWaitKey(0);
        }
        else if(dirimages==0){
            std::cout << "this directory doesnt contain images" <<"\n";
        }

    }else{

        RecurseDirectory(QDir::currentPath());
        //             QDir dir=QDir::currentPath();
        //             dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
        //             dir.setSorting(QDir::Size | QDir::Reversed);

        //             QFileInfoList list = dir.entryInfoList();
        //             std::cout << "     Bytes Filename" << std::endl;
        //             for (int i = 0; i < list.size(); ++i) {
        //                 QFileInfo fileInfo = list.at(i);
        //                 std::cout << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10)
        //                                                         .arg(fileInfo.absoluteFilePath()));
        //                 std::cout << std::endl;
        //             }
    }
    return 0;
}
//void myclass{}

