#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
aa_index
create an index HTML+RDFa of a directory of files
Creates nodes based on:
* SHA1 hash of file contents (git compatible)
** Filesize
** Absolute path to file at time of snapshot
** Extension

no this is the accumulate only run

"""

import os, sys
import xml.sax.saxutils
from common import *
import settings


try:
    path = sys.argv[1]
except IndexError:
    path = "."

path = os.path.abspath(path)
recurse = True

def escape (s):
    return xml.sax.saxutils.escape(s)


### TODO: Add start / end timestamp of scripts execution (and maybe a content hash of the producing script!)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	                                            
def rdfdoc(body):
    doc = """<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml"
    xmlns:aa="http://activearchives.org/terms/"
    xmlns:ekn="http://kurenniemi.activearchives.org/nodes/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dcterms="http://purl.org/dc/terms/"
    version="HTML+RDFa 1.1">
<head>
<title>active archives: index</title>
<meta charset="utf-8" />
</head>
<body>
"""
    doc += body
    doc += """
</body>
</html>
"""
    return doc
    
for root, dirs, files in os.walk(path):
    print "Indexing directory", root

    ## DONT PROCESS .aa folders
    if ".aa" in dirs:
        dirs.remove(".aa")

    # ENSURE .aa folder
    aapath = os.path.join(root, ".aa")
    try:
        os.mkdir(aapath)
    except OSError:
        pass

    html = "<table>\n"
    for f in files:
        if f.startswith("."): continue
        apath = os.path.abspath(os.path.join(root, f))
        # relpath = os.path.relpath(apath, settings.ROOT_PATH)
        fs = os.path.getsize(apath)
        (dpath, fname) = os.path.split(apath)
        (fname, ext) = os.path.splitext(fname)
        ext = ext.lstrip(".").lower() # NB FORCE LOWERCASE
        fsize = os.path.getsize(apath)
        # (_, pname) = os.path.split(dpath)
        sha1 = githash(apath)

        html += '<tr about="[ekn:%s]">\n' % sha1
        # print '    <td>%s</td>' % relpath
        # print '    <td property="aa:pname">%s</td>\n' % pname
        html += '    <td property="aa:fname">%s</td>\n' % escape(fname)
        html += '    <td property="aa:ext">%s</td>\n' % escape(ext)
        html += '    <td property="aa:fsize" datatype="xsd:integer">%s</td>\n' % fsize
        html += '    <td property="aa:path">%s</td>\n' % escape(apath)
        html += '</tr>\n'

    html += "</table>\n"

    htmlpath = os.path.join(aapath, "index.html")
    out = open(htmlpath, "wb")
    out.write(rdfdoc(html))
    out.close()

    if not recurse: break


