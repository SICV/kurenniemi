<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link href='http://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'><link rel='stylesheet' type='text/css' media='all' href='extensions/porte_plume/css/barre_outils.css' />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script src="/code/lib/jquery/jquery.js"></script>
<script src="/code/lib/idleplugin/jquery.idle-timer.js"></script>

<script src="<?php bloginfo('template_url'); ?>/script.js"></script>
<script>
var cnt=0;
var ctrl=1;
var tempo=10000;
var MAXTIMEOUT=300000;//(5x60x1000=300000=5mins)
<?php
if(is_single()){
    $timeout = get_post_meta($post->ID, "timeout", true);
    if($timeout){
      echo "var timeout=".$timeout.";\n";
      echo "var maxed=0;\n";

    }else{
      echo "var timeout=MAXTIMEOUT;\n";
      echo "var maxed=1;\n";


}
      }
 ?>



</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body>
<div id="page2048">
<div id="timeout"></div>
<div id="msg_timer"></div><div id="alert_msg"></div>
	<div class="header2048">

					<span class="sitetitle2048">
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="homelink2048"><?php bloginfo( 'name' ); ?></a>
					</span>
<span>
                                         <?php bloginfo( 'description' ); ?>
</span>
	</div><!-- #header -->
<div id="cursor2048">
<?php 
	  if(is_single()){  echo '<span class="cursorbar2048">';previous_post_link('%link', '&larr;'); echo '</span>';} 

	  $args = array( 'numberposts' => -1,'order'=>'asc');
$posts_array = get_posts( $args ); 
foreach($posts_array as $postmp) : 
        $class='linkbar2048';
        if($post->ID==$postmp->ID){
          $class='linkbaron2048';

}
?>

	<span class="cursorbar2048"><a href="?p=<?php echo $postmp->ID; ?>" class="<?php echo $class; ?>">|</a></span>
<?php
endforeach; 
if(is_single()){ echo '<span class="cursorbar2048">';next_post_link('%link', '&rarr;'); echo '</span>';} 
?> 

</div>

