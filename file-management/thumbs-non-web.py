import os,shutil
from PIL import Image
testdir="./ERKKI"
copydir="./ERKKI-128x128-nonweb"
i=0
j=0
formats=['GIF','JPEG','PNG','TIFF']
extensionspil=['.gif','.jpg','.jpeg','.jpe','.png']
extensionsimagick=['.psd','.tiff','.tif']
for root, dirs, files in os.walk(testdir):
    print root
    copyroot=copydir+root.replace('.','')
    if os.path.isdir(copyroot) == False:
        print copyroot+" must be created"
        os.makedirs(copyroot)
    #print root, "files", files
    for f in files:
        #print f
        fname, ext = os.path.splitext(f)
        #print ext
        if ext.lower() in extensionsimagick:
            rootfile=root+'/'+f
            copyfile=copyroot+'/'+f
            print rootfile
            print copyfile
            if os.path.exists(copyfile)==False:
                print rootfile +" must be copied and resized"
                try:
                    shutil.copy2(rootfile,copyfile)
                    if ext.lower()=='.psd' or ext.lower()=='.tiff' or ext.lower()=='.tif':
                        os.system('mogrify -resize 128 -format jpg '+copyfile.replace(' ','\ '))
                        os.system('rm '+copyfile.replace(' ','\ '))
                        j+=1
                except:
                    print 'error copying file'

    i+=1

print 'Number of directories '+str(i)    
print 'Number of images jpg '+str(j)

