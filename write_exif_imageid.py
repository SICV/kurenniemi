#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os, sys, hashlib
import pyexiv2


IMAGEID = 'Exif.Image.ImageID'

def githash(path, block_size=2**20):
    """ Should produce the same SHA1 hash git uses for a particular file """
    s = hashlib.sha1()
    filesize = os.path.getsize(path)
    s.update("blob %u\0" % filesize)
    f = open(path, "rb")
    while True:
        data = f.read(block_size)
        if not data:
            break
        s.update(data)
    return s.hexdigest()

def write_exif_imageid (fromfile, tofile):
    sha1 = githash(fromfile)
    meta = pyexiv2.ImageMetadata(tofile)
    meta.read()
    meta[IMAGEID] = pyexiv2.ExifTag(IMAGEID, sha1)
    meta.write()

try:
    original = sys.argv[1]
    derivative = sys.argv[2]
    if os.path.isdir(original) and os.path.isdir(derivative):
        # process directories
        for o in os.listdir(original):
            opath = os.path.join(original, o)
            # look for corresponding file in thumbs
            tpath = os.path.join(derivative, o)
            if os.path.exists(tpath):
                write_exif_imageid(opath, tpath)
    else:
        write_exif_imageid(original, derivative)
except IndexError:
    print "usage: python write_exif_imageid.py original-item-or-folder thumb-item-or-folder"




