#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sqlite3, sys, datetime, cgi, re
from settings import DBNAME, STOP_KEYS, DAYSTART_HOUR


fs = cgi.FieldStorage()
d = fs.getvalue("d")
if d == None:
    day = datetime.datetime.today()
    # get rid of time
    day = datetime.datetime(day.year, day.month, day.day, DAYSTART_HOUR)
else:
    y, m, d = d.split("-")
    day = datetime.datetime(int(y), int(m), int(d), DAYSTART_HOUR)

def parse_values (val):
    lines = val.splitlines()
    data = {}
    keyvals = []
    data['keyvals'] = keyvals
    for line in lines:
        line = line.strip()
        if line and ":" in line:
            (key, value) = line.split(":", 1)
            if key == 'title':
                data['title'] = value
            elif key not in STOP_KEYS:
                keyvals.append((key, value))
    return data

dtpat = re.compile(r"(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)")
def parsedatetime (tstr):
    m = dtpat.search(tstr)
    if m:
        y = int(m.group(1))
        month = int(m.group(2))
        d = int(m.group(3))
        h = int(m.group(4))
        mins = int(m.group(5))
        s = int(m.group(6))
        return datetime.datetime(y, month, d, h, mins, s)

def timelabel(t):
    lt = timelabel.lasttime
    if lt and lt.hour == t.hour and lt.minute == t.minute:
        ret = ""
    else:
        local_t = t + datetime.timedelta(0, 3600)
        ret = "{0:02d}:{1:02d}".format(local_t.hour, local_t.minute)

    timelabel.lasttime = t
    return ret
timelabel.lasttime = None
    

print """Content-type: text/html;charset=utf-8"""
print

print """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Erkki Kurenniemi: In 2048: Dataradio</title>
<link rel="stylesheet" href="/dataradio/styles.css" type="text/css" charset="utf-8" />
<style>
table {}
td {
    vertical-align: top;
    padding: 5px;
}
</style>
</head>
<body class="side">
"""

daydelta = datetime.timedelta(1)
day_m1 = day - daydelta
# day_m2 = day_m1 - daydelta
day_p1 = day + daydelta
day_p2 = day_p1 + daydelta

conn = sqlite3.connect(DBNAME)
c = conn.cursor()

# PREV
prev = ""
# check if prevday has data
q = "SELECT id FROM erkki_messages WHERE tstamp>=? AND tstamp<? LIMIT 1"
count = 0
for row in c.execute(q, (day_m1, day)):
    count += 1
if count:
    prev = """<a href="?d={0:04d}-{1:02d}-{2:02d}">&lt;</a> """.format(day_m1.year, day_m1.month, day_m1.day)

# NEXT
next = ""
# check if nextday has data
q = "SELECT id FROM erkki_messages WHERE tstamp>=? AND tstamp<? LIMIT 1"
count = 0
for row in c.execute(q, (day_p1, day_p2)):
    count += 1
if count:
    next = """ <a href="?d={0:04d}-{1:02d}-{2:02d}">&gt;</a>""".format(day_p1.year, day_p1.month, day_p1.day)

# OUTPUT HEADER
print """<p><b>schedule: {0}{1}{2}</b></p>""".format(prev, day.strftime("%d-%m-%Y"), next)

# QUERY FOR TODAY
q = "SELECT type, message, tstamp FROM erkki_messages WHERE tstamp>=? AND tstamp <? ORDER BY id"
rows = c.execute(q, (day, day_p1))

print """<table cellspacing="0" cellpadding="0">"""
for row in rows:
    tstamp = parsedatetime(row[2])
    msg = row[1].strip()
    data = parse_values(msg)
    print """<tr><td>{0}</td>""".format(timelabel(tstamp))
    print """<td>""" 
    if 'title' in data:
        print "<b>{0}</b><br />".format(data['title'])
    print "<br />\n".join(["{0}: {1}".format(x[0], x[1]) for x in data['keyvals']]) 
print """</table>"""

conn.close()


print """</body>
</html>
"""
