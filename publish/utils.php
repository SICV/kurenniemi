<?php

function list_dir($d,$order='alpha',$filter='all'){
  //print $d."\n";
  $ls=array();
  $ls_dirs=array();
  $ls_files=array();
  if($order=='alpha'){
    $files = scandir("$d");
  }else if($order=='desc'){
    $files = scandir($d,1);

  }else{
    print 'not order set';
    return false;
}
  foreach($files as $f){
    $endphp='/\.php~?/i';
    $endln='/\.ln/i';
    if(($f!='.')&&($f!='..')&&(!preg_match($endphp,$f))&&($f!='thumbsforpreview')){
      if(is_dir($d.'/'.$f)){
	$type='dir';
      }else{
	$type='file';
      }
      if(is_dir($d.'/'.$f)){
	$ls_dirs[]=array('name'=>$f,'public'=>'y','type'=>$type);
      }else{
	if(preg_match($endln,$f)){
	if($filter!='public'){
	  $f=preg_replace($endln,'',$f);
	  $ls_files[]=array('name'=>$f,'public'=>'y','type'=>$type);
}
	}else{
	  if($filter!='hidden'){
	  $ls_files[]=array('name'=>$f,'public'=>'n','type'=>$type);

}
}
}

      /* if(check_public($d,$f)&&($filter!='hidden')){ */
      /* 	if(is_dir($d.'/'.$f)){ */
      /* 	  $ls_dirs[]=array('name'=>$f,'public'=>'y','type'=>$type); */
      /* 	}else{ */
      /* 	  $ls_files[]=array('name'=>$f,'public'=>'y','type'=>$type); */

      /* 	} */
      /* }else if((check_public($d,$f)===false) &&($filter!='public')){ */
      /* 	if(is_dir($d.'/'.$f)){ */
      /* 	  $ls_dirs[]=array('name'=>$f,'public'=>'n','type'=>$type); */
      /* 	}else{ */
      /* 	  $ls_files[]=array('name'=>$f,'public'=>'n','type'=>$type); */
      /* 	} */
      /* } */
    }}
  $ls=array_merge($ls_dirs,$ls_files);
  return $ls;
}
function check_public($d,$f){
  global $basepublicd;
  $publicfile=$basepublicd.'/'.$d.'/'.$f;
  if(file_exists($publicfile)){
    return true;
  }else{
    return false;
}

}
function toggle_public($d,$f){
  global $basepublicd,$based;
  $privatefile=$based.'/'.$d.'/'.$f;
  $publicfile=$basepublicd.'/'.$d.'/'.$f;
  if(file_exists($publicfile)){
    unlink($publicfile);
  }else{
    copy($privatefile,$publicfile);
}


}
function dir_link($d){

}


function bc_getparts($d){
  global $based;
  $parts_base=split('/',$based);
  $parts_new=split('/',$d);
  /* print_r($parts_base); */
  /* print_r($parts_new); */
  $parts=array_diff($parts_new,$parts_base);
  return $parts;
}

function breadcrumb($d,$addstr=''){
  /* global $based; */
  $str_out='<div class="nav">Trail ';
  $parts=bc_getparts($d);
  $pp='';
  $cnt=count($parts);
  $i=0;
  foreach($parts as $part){
    $pp.=urlencode($part).'/';
    $fname=($part=='.'? 'Uploads':$part);
    if($i<($cnt-1)){
      $str_out.='&middot; <a href="ls.php?d='.$pp.$addstr.'">'.$fname.'</a> ';
    }else{
      $str_out.='&middot; '.$fname.' ';

    }
    $i++;
  }
  $str_out.='</div>';
  return $str_out;
}

function urlencode_parts($d){
  $str_out='';
  $parts=bc_getparts($d);
  $cnt=count($parts);
  $i=0;
  foreach($parts as $part){
    if($i<($cnt-1)){
      $str_out.=urlencode($part).'/';
    }else{
      $str_out.=urlencode($part);

    }
    $i++;
  }
  return $str_out;
}
function do_ln($f,$d){
  global $basepublicd;
  if(($d!='.')&&($d!='./')&&($d!='')){
    $file_in=$d.'/'.$f;
    $address =  $basepublicd.'/'.$d.'/'.$f;
  }else{
    $file_in=$f;
    $address =  $basepublicd.'/'.$f;
  }

  $filename = $file_in.'.ln';

  if (!$handle = fopen($filename, 'w')) {
    echo "Cannot open file ($f)";
    return false;
  }
  if (fwrite($handle, $address) === FALSE) {
    echo "Cannot write to file ($f)";
    return false;
  }
  echo "Success, wrote $address to file $f";
  fclose($handle);
  return array($filename,$file_in,$address);
}



/* function delete_ln(){ */

/*   global $basepublicd,$based; */
/*   if(($d!='.')&&($d!='./')&&($d!='')){ */
/*     $file_in=$d.'/'.$f; */
/*   }else{ */
/*     $file_in=$f; */
/*   } */
/*   $filename = $file_in.'.ln'; */
/*   if($address=file_get_contents($filename){ */
/*       $publicprefix=$basepublicd.'/'; */
/*       $private_address=ereg_replace($publicprefix,'',$address); */
/*       rename($address,$private_address); */
/*       print "Renamed $address in $private_address\n"; */
/*       unlink($filename); */
/*       print "Deleted link file $filename\n"; */
/* } */
/*     return $address; */
/* } */

function mirror_dirs($pathname){
  // Check if directory already exists
  if (is_dir($pathname) || empty($pathname)) {
    return true;
  }
  // Crawl up the directory tree
 
  $next_pathname = substr($pathname, 0, strrpos($pathname, DIRECTORY_SEPARATOR));
  if (mirror_dirs($next_pathname)) {
    if (!file_exists($pathname)) {
      return mkdir($pathname);
    }
  }
  return true;
 
}

function move_file($filename,$address){
  $paths=split('/',$address);
  $f=array_pop($paths);
  $path=implode('/',$paths);
  if(mirror_dirs($path)===false){
    return false;
  }else{
    if(rename($filename,$address)===false){
      return false;
    }else{
      return true;
    }}
}
function make_public($f,$d){
  if(!(list($file,$file_in,$address)=do_ln($f,$d))){
    return false;
  }else{
    if(!move_file($file_in,$address)){
      echo "file $file_in could not be moved to $address\n";
      return false;
    }else{
      echo "file $file_in has been moved to $address\n";

    }
  }
  return $address;
}
function make_private($f,$d){
  global $basepublicd,$based;
  if(($d!='.')&&($d!='./')&&($d!='')){
    $file_in=$d.'/'.$f;
  }else{
    $file_in=$f;
  }
  $filename = $file_in.'.ln';
  if($address=file_get_contents($filename)){
      $publicprefix=$basepublicd.'/';
      $private_address=ereg_replace($publicprefix,'',$address);
      rename($address,$private_address);
      print "Renamed $address in $private_address\n";
      unlink($filename);
      print "Deleted link file $filename\n";
}
    return $private_address;

}
?>